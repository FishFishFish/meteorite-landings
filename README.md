# Meteorite-landings #

Dataset source: https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfh

## Meteorite-landings ##
![Screenshot_20170421_213159.png](https://bitbucket.org/repo/7EKLbXy/images/3001782092-Screenshot_20170421_213159.png)

![BoxPlot.png](https://bitbucket.org/repo/7EKLbXy/images/3205643093-BoxPlot.png)

![Density.png](https://bitbucket.org/repo/7EKLbXy/images/3711672054-Density.png)

![Largest.png](https://bitbucket.org/repo/7EKLbXy/images/3984092612-Largest.png)

![Mass.png](https://bitbucket.org/repo/7EKLbXy/images/2020636151-Mass.png)